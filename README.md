Spring Boot and Vue demo application.

Instructions
--
1)    Compile the project:
      
          mvn clean install
      
      The Vue application (in /spa) will be compiled using frontend-maven-plugin, and the resulting files copied to the /api/src/main/resources/public folder
   
2)    Run the application:
      
          mvn spring-boot:run -pl api
      
      The Spring Boot application uses an in-memory H2 database which is populated on startup. The Vue app (accessible on  http://localhost:8080) will retrieve data using REST API calls.


For books with an ISBN the app attempts to load a cover image from [Open Library](https://openlibrabry.org)