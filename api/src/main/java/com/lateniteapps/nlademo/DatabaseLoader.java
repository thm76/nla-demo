package com.lateniteapps.nlademo;

import com.lateniteapps.nlademo.model.Book;
import com.lateniteapps.nlademo.model.Person;
import com.lateniteapps.nlademo.repository.BookRepository;
import com.lateniteapps.nlademo.repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class DatabaseLoader implements CommandLineRunner {
    private final PersonRepository personRepository;

    private final BookRepository bookRepository;

    @Autowired
    public DatabaseLoader(PersonRepository personRepository, BookRepository bookRepository) {
        this.personRepository = personRepository;
        this.bookRepository = bookRepository;
    }


    @Override
    public void run(String... args) throws Exception {
        Person[] persons = new Person[]{
                new Person("Malcolm Reynolds", "123", "mal@firefly.ship"),
                new Person("Zoe Washburn", "234", "zoe@firefly.ship"),
                new Person("Hoban Washburn", "234", "wash@firefly.ship"),
                new Person("Inara Serra", "666", "hello@inara.com"),
                new Person("Jayne Cobb", "678", "jayne@firefly.ship"),
                new Person("Kaylee Frye", "987", "kaylee@firefly.ship"),
                new Person("Simon Tam", "876", "simon@firefly.ship"),
                new Person("River Tam", "876", null),
                new Person("Derrial Book", "765", null),
        };

        Book[] books = new Book[]{
                new Book("A Game of Thrones", "George R. R. Martin", "9780553573404", persons[2]),
                new Book("A Dance with Dragons", "George R. R. Martin", "9780553801477", persons[0])
        };

        for (Person person : persons) {
            this.personRepository.save(person);
        }

        for (Book book : books) {
            this.bookRepository.save(book);
        }
    }
}
