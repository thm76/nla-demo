package com.lateniteapps.nlademo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NlaDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(NlaDemoApplication.class, args);
    }
}
