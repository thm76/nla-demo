package com.lateniteapps.nlademo.controller;

import com.lateniteapps.nlademo.model.Book;
import com.lateniteapps.nlademo.model.Person;
import com.lateniteapps.nlademo.repository.BookRepository;
import com.lateniteapps.nlademo.repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.NonNull;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api")
@CrossOrigin()
public class NLARestController {

    private PersonRepository personRepository;

    private BookRepository bookRepository;

    @Autowired
    public NLARestController(PersonRepository personRepository, BookRepository bookRepository) {
        this.personRepository = personRepository;
        this.bookRepository = bookRepository;
    }

    @GetMapping("/persons")
    private ResponseEntity<List<Person>> getPersons() {
        List<Person> people = new ArrayList<>();
        personRepository.findAll().forEach(people::add);
        return new ResponseEntity<>(people, HttpStatus.OK);
    }

    @GetMapping("/persons/{id}/books")
    private ResponseEntity<List<Book>> getCheckedOutBooks(@PathVariable("id") Long personId) {
        Person person = personRepository.findById(personId).orElse(null);

        if (person == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        List<Book> books = new ArrayList<>(bookRepository.findByCheckedOutByPerson(person));

        return new ResponseEntity<>(books, HttpStatus.OK);
    }

    @GetMapping("/books")
    private ResponseEntity<List<Book>> getBooks() {
        List<Book> books = new ArrayList<>();
        bookRepository.findAll().forEach(books::add);
        return new ResponseEntity<>(books, HttpStatus.OK);
    }
}
