package com.lateniteapps.nlademo.model;

import javax.persistence.*;

@Entity
public class Book  {
    @Id
    @GeneratedValue
    private Long id;
    private String title;
    private String author;
    private String isbn;
    @ManyToOne(fetch = FetchType.EAGER)
    private Person checkedOutByPerson;

    public Book() {
    }

    public Book(String title, String author, String isbn, Person checkedOutByPerson) {
        this.title = title;
        this.author = author;
        this.isbn = isbn;
        this.checkedOutByPerson = checkedOutByPerson;
    }

    public Person getCheckedOutByPerson() {
        return checkedOutByPerson;
    }

    public void setCheckedOutByPerson(Person checkedOutByPerson) {
        this.checkedOutByPerson = checkedOutByPerson;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }
}
