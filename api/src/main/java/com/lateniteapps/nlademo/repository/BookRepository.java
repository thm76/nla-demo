package com.lateniteapps.nlademo.repository;

import com.lateniteapps.nlademo.model.Book;
import com.lateniteapps.nlademo.model.Person;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BookRepository extends CrudRepository<Book, Long> {
    List<Book> findByCheckedOutByPerson(Person person);
}
