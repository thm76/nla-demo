import Vue from 'vue'
import BootstrapVue from 'bootstrap-vue'
import App from './App.vue'
import PeopleList from './components/PeopleList'
import router from './router'

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import BooksList from './components/BooksList'

Vue.config.productionTip = false

Vue.use(BootstrapVue)

new Vue({
  router,
  components: {
    PeopleList,
    BooksList
  },
  render: h => h(App)
}).$mount('#app')
