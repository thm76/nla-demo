import Vue from 'vue'
import Router from 'vue-router'
import People from './views/People'
import Books from './views/Books'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '*',
      redirect: '/persons'
    },
    {
      path: '/persons',
      name: 'persons',
      component: People
    },
    {
      path: '/books',
      name: 'books',
      component: Books
    }
  ]
})
